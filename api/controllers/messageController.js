'use strict';


var mongoose = require('mongoose'),
    Message = mongoose.model('Messages');

// Message importer
exports.import_messages = function (req, res) {
    var messages = req.body.messages;
    var messageCount = messages.length;

    for (var index = 0; index < messageCount; index++) {
        var new_message = new Message(messages[index]);
        new_message.is_read = false;
        new_message.is_archived = false;

        new_message.save(function (err, message) {
            if (err) {
                console.log(err);
            }
        });
    };

    // As the saving mechanism is asynchronous, so we will accept the request only. 
    // It's possible to inform the user about the status of the process with the response, 
    // I think it wouldn't be a good idea to hault the user for a long time as the import process may need to handle huge volume of data.
    // Obviously there should be a way for users to know about the status of the import process. I would prefer to have a job status api endpoint.
    // Alternatively we can also ask for a callback URI from the user depending on the requirement.
    res.status(202).send({ message: 'Import request is accepted for further processing' });
};

// Retrieves all messages
// Supports pagination. By default the limit is 10.
// Retrieved messages are sorted based on the sent time.
exports.list_all_message = function (req, res) {
    var query = Message.find({}, { "is_archived": false, "_id": false, "__v": false });

    var limit = parseInt(req.query.top, 10);
    if (isNaN(limit) || limit < 1) {
        limit = 10;
    }

    var offset = parseInt(req.query.skip, 10);
    if (isNaN(offset) || offset < 1) {
        offset = 0;
    }

    Message.paginate(query, { offset: offset, limit: limit, sort: { time_sent: -1 } }, function (err, result) {
        if (err)
            res.send(err);
        else {
            res.json(result);
        }
    });
};

// Retrieves all archived messages
// Supports pagination and records are sorted based on the the sen time.
exports.list_all_archived_message = function (req, res) {
    var query = Message.find({ is_archived: true }, { "is_archived": false, "_id": false, "__v": false });

    var limit = parseInt(req.query.top, 10);
    if (isNaN(limit) || limit < 1) {
        limit = 10;
    }

    var offset = parseInt(req.query.skip, 10);
    if (isNaN(offset) || offset < 1) {
        offset = 0;
    }

    Message.paginate(query, { offset: offset, limit: limit, sort: { time_sent: -1 } }, function (err, result) {
        if (err)
            res.send(err);
        else {
            res.json(result);
        }
    });
};

// Retireves a specific message by the UID.
exports.get_a_message = function (req, res) {
    Message.findOne({ uid: req.params.messageId }, { "_id": false, "__v": false }, function (err, message) {
        if (err)
            res.send(err);
        else {
            res.json(message);
        }
    });
};

// Sets a specific message as read
exports.read_message = function (req, res) {
    Message.findOneAndUpdate({ uid: req.params.messageId }, { "$set": { "is_read": true } }, { new: true }).exec(function (err, message) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send({ message: 'Successfully updated the message read status' });
        }
    });
};

// Archives a specific message
exports.archive_message = function (req, res) {
    Message.findOneAndUpdate({ uid: req.params.messageId }, { "$set": { "is_archived": true } }, { new: true }).exec(function (err, message) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send({ message: 'Successfully archived message' });
        }
    });
};