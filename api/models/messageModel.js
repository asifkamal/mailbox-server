'use strict';

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;


var MessageSchema = new Schema({
  uid: {
    type: Number
  },
  sender: {
    type: String
  },
  subject: {
    type: String
  },
  message: {
    type: String
  },
  time_sent: {
    type: Date
  },
  is_read:  {
    type: Boolean
  },
  is_archived:  {
    type: Boolean
  }
});

MessageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Messages', MessageSchema);