'use strict';
module.exports = function (app) {
    var message = require('../controllers/messageController');

    // message Routes
    app.route('/api/messages')
        .get(message.list_all_message);

    app.route('/api/messages/archived')
        .get(message.list_all_archived_message);

    app.route('/api/messages/:messageId')
        .get(message.get_a_message);

    app.route('/api/messages/:messageId/archive')
        .post(message.archive_message);

    app.route('/api/messages/:messageId/read')
        .post(message.read_message);

    app.route('/api/import')
        .post(message.import_messages);
};