var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  mongoosePaginate = require('mongoose-paginate'),
  Message = require('./api/models/messageModel'), //created model loading here
  bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Messagedb');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//importing route
var routes = require('./api/routes/messageRoute');

//register the route
routes(app);

app.listen(port);

console.log('MailBox RESTful API server started on: ' + port);