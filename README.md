# MailBox API

Tools Required:

 * Node.js
 * MongoDB

Following packages are used:

 * body-parser (for parsing incoming requests)
 * express (to make the application run)
 * nodemon (restarting server when changes occur)
 * mongoose (object data modeling to simplify interactions with MongoDB)
 * mongoose-paginate (for supporting pagination)

How to run:

 * Install the required packages through commandline using **npm-install-all** command
 * Run the server using the command **npm run start**

### API Specification:

The RESTful API supports basic mail box operations to manage internal messaging of a local firm. The API listens to 3000 port. Following use cases are supported through the API:


#### Retrieve List of Messages
Retrieves list of all messages. This end-point is pagination supported. The results are sorted based on the sent time field. If no top & skip value is provided then 10 records will be retrieved. The response includes only the read status.

GET api/messages

Example: http://localhost:3000/api/messages?top=2&skip=1

Response Example: 

Status: 200 OK

```json
{  
   "docs":[  
      {  
         "is_read":false,
         "uid":22,
         "sender":"Stephen King",
         "subject":"adoration",
         "message":"The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
         "time_sent":"1970-01-17T21:20:48.747Z"
      },
      {  
         "is_read":false,
         "uid":21,
         "sender":"Ernest Hemingway",
         "subject":"animals",
         "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
         "time_sent":"1970-01-17T21:20:39.867Z"
      }
   ],
   "total":6,
   "limit":2,
   "offset":1
}

```

#### Retrieve List of Archived Messages

Retrieves list of all archived messages. Pagination is also supported. The results are sorted based on the sent time field. If no top & skip value is provided then 10 records will be retrieved. The response includes only the read status.

GET api/messages/archived

Example: http://localhost:3000/api/messages/archived?top=2

Response Example:

Status: 200 OK
```json
{  
   "docs":[  
      {  
         "is_read":false,
         "uid":22,
         "sender":"Stephen King",
         "subject":"adoration",
         "message":"The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
         "time_sent":"1970-01-17T21:20:48.747Z"
      },
      {  
         "is_read":false,
         "uid":21,
         "sender":"Ernest Hemingway",
         "subject":"animals",
         "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
         "time_sent":"1970-01-17T21:20:39.867Z"
      }
   ],
   "total":2,
   "limit":2,
   "offset":
}
```

#### Retrieve a specific message by UID

This API end point retrieves a specific message by UID. If a message with the specified UID is not found then null is returned for the prototype phase. The response includes the read and archived status.

GET api/messages/{UID} 

Example: http://localhost:3000/api/messages/21

Response Example:
```json
Status : 200 OK

{  
   "is_archived":true,
   "is_read":false,
   "uid":21,
   "sender":"Ernest Hemingway",
   "subject":"animals",
   "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
   "time_sent":"1970-01-17T21:20:39.867Z"
}
```

#### Mark a Message as Read

This endpoint marks a specific message as read.

POST api/messages/{UID}/read

The payload does not have much significance here. So empty payload is allowed.

Example: http://localhost:3000/api/messages/21/read

Response Example:

```json
Status: 200 OK
{  
   "message":"Successfully updated the message read status"
}
```

#### Archive a Message

This endpoint marks a specific message as archived.

POST api/messages/{UID}/archive

The payload does not have much significance here. So empty payload is allowed.

Example: http://localhost:3000/api/messages/21/archive

Response Example:

Status: 200 OK
```json
{  
   "message":"Successfully archived message"
}
```


#### Import Messages

This endpoint allows to import messages. The payload is expected to be a JSON object. The server returns an Accepted response immediately after receiving the request and then starts to process the request. For the prototype phase there is no way to check the status of the import process.

POST api/import

Payload Example:
```json
{
  "messages": [
    {
      "uid": "21",
      "sender": "Ernest Hemingway",
      "subject": "animals",
      "message": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
      "time_sent": 1459239867
    },
    {
      "uid": "22",
      "sender": "Stephen King",
      "subject": "adoration",
      "message": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
      "time_sent": 1459248747
    },
    {
      "uid": "23",
      "sender": "Virgina Woolf",
      "subject": "debt",
      "message": "The story is about an obedient midwife and a graceful scuba diver who is in debt to a fence. It takes place in a magical part of our universe. The story ends with a funeral.",
      "time_sent": 14567678677
      
    }
  ]
}
```

Sample Response:

Status: 202 ACCEPTED

```json
{  
   "message":"Import request is accepted for further processing"
}
```


### Assumptions:
Following assumptions were made during the development phase:

* Instead of creating a separate task for importing, adding an API end-point is sufficient for the import task.
* Import data will be in correct format for the proto-type.
* UID for importing messages will always be a number.
* List all messages will include the archived messages as well.
* Pagination will be determined through the top & skip query parameter.
* Default limit for pagination is 10.

### Approach for production-ready system:
Following things must be addressed for the production-ready system

* Logging.
* Introducing service layer for database communications.
* Adding middlewares to support logging, error handling.
* Supporting authentication & authorization.
* More specific HTTP status codes as response (NotFound, InteralServerError, BadRequest).
* Informing user about the status of import process (may be through callback URI or job status api).
* Data validation.
* Adding more comments where required.
* Adding unit tests.
